package com.example.birasanthpushpanathan.sarmilanwedsanujah;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{

	private ArrayList<Button> listOfButtons;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initializeComponents();

	}

	public void onClick(View view) throws ColorNotDefineableException
	{
		Button clickedButton = ((Button) view);

		boolean colorIsRed = getToggledValueForButton(clickedButton);

		if (colorIsRed)
		{
			clickedButton.setBackgroundColor(getGreenColor(R.color.green));

		} else
		{
			clickedButton.setBackgroundColor(getRedColor());
		}
	}

	private boolean getToggledValueForButton(Button clickedButton)
	{
		boolean result = false;

		try
		{
			return isColorRedForButton(clickedButton);
		} catch (RippleDrawableWasTheWrongDrawable rippleDrawableWasTheWrongDrawable)
		{

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		try
		{
			return isColorRedForButtonColorDrawable(clickedButton);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}


	private boolean isColorRedForButton(Button clickedButton)
			throws RippleDrawableWasTheWrongDrawable
	{
		boolean colorIsRed = false;
		try
		{

			RippleDrawable rippleDrawable = (RippleDrawable) clickedButton.getBackground();

			Drawable.ConstantState state = rippleDrawable.getConstantState();
			Field colorField = state.getClass().getDeclaredField("mColor");
			colorField.setAccessible(true);

			ColorStateList colorStateList = (ColorStateList) colorField.get(state);
			int rippleColor = colorStateList.getDefaultColor();

			colorIsRed = (rippleColor == getRedColor());
			return colorIsRed;
		} catch (Exception e)
		{
			throw new RippleDrawableWasTheWrongDrawable();

		}

	}


	private boolean isColorRedForButtonColorDrawable(Button clickedButton)
			throws ColorNotDefineableException
	{
		try
		{
			ColorDrawable buttonColor = (ColorDrawable) clickedButton.getBackground();
			int colorId = buttonColor.getColor();

			return (colorId == getRedColor());
		} catch (Exception e1)
		{
			throw new ColorNotDefineableException();
		}

	}

	public void initializeComponents()
	{
		listOfButtons = new ArrayList<>();

		listOfButtons.add((Button) findViewById(R.id.table));
		listOfButtons.add((Button) findViewById(R.id.table2));
		listOfButtons.add((Button) findViewById(R.id.table3));
		listOfButtons.add((Button) findViewById(R.id.table4));
		listOfButtons.add((Button) findViewById(R.id.table5));
		listOfButtons.add((Button) findViewById(R.id.table6));
		listOfButtons.add((Button) findViewById(R.id.table7));
		listOfButtons.add((Button) findViewById(R.id.table8));
		listOfButtons.add((Button) findViewById(R.id.table9));
		listOfButtons.add((Button) findViewById(R.id.table10));
		listOfButtons.add((Button) findViewById(R.id.table11));
		listOfButtons.add((Button) findViewById(R.id.table12));
		listOfButtons.add((Button) findViewById(R.id.table13));
		listOfButtons.add((Button) findViewById(R.id.table14));
		listOfButtons.add((Button) findViewById(R.id.table15));
		listOfButtons.add((Button) findViewById(R.id.table16));
		listOfButtons.add((Button) findViewById(R.id.table17));
		listOfButtons.add((Button) findViewById(R.id.table18));
		listOfButtons.add((Button) findViewById(R.id.table19));
		listOfButtons.add((Button) findViewById(R.id.table20));
		listOfButtons.add((Button) findViewById(R.id.table21));
		listOfButtons.add((Button) findViewById(R.id.table22));
		listOfButtons.add((Button) findViewById(R.id.table23));
		listOfButtons.add((Button) findViewById(R.id.table24));
		listOfButtons.add((Button) findViewById(R.id.table25));
		listOfButtons.add((Button) findViewById(R.id.table26));
		listOfButtons.add((Button) findViewById(R.id.table27));
		listOfButtons.add((Button) findViewById(R.id.table28));
		listOfButtons.add((Button) findViewById(R.id.table29));
		listOfButtons.add((Button) findViewById(R.id.table29));
		listOfButtons.add((Button) findViewById(R.id.table30));
		listOfButtons.add((Button) findViewById(R.id.table31));
		listOfButtons.add((Button) findViewById(R.id.table32));
		listOfButtons.add((Button) findViewById(R.id.table33));
		listOfButtons.add((Button) findViewById(R.id.table34));
		listOfButtons.add((Button) findViewById(R.id.table35));
		listOfButtons.add((Button) findViewById(R.id.table36));
		listOfButtons.add((Button) findViewById(R.id.table37));
		listOfButtons.add((Button) findViewById(R.id.table38));
		listOfButtons.add((Button) findViewById(R.id.table39));
		listOfButtons.add((Button) findViewById(R.id.table40));
		listOfButtons.add((Button) findViewById(R.id.table41));
		listOfButtons.add((Button) findViewById(R.id.table42));
		listOfButtons.add((Button) findViewById(R.id.table43));
		listOfButtons.add((Button) findViewById(R.id.table43));
		listOfButtons.add((Button) findViewById(R.id.table44));
		listOfButtons.add((Button) findViewById(R.id.table45));
		listOfButtons.add((Button) findViewById(R.id.table46));
		listOfButtons.add((Button) findViewById(R.id.table47));
		listOfButtons.add((Button) findViewById(R.id.table48));
		listOfButtons.add((Button) findViewById(R.id.table49));
		listOfButtons.add((Button) findViewById(R.id.table50));
		listOfButtons.add((Button) findViewById(R.id.table51));
		listOfButtons.add((Button) findViewById(R.id.table52));
		listOfButtons.add((Button) findViewById(R.id.table53));
		listOfButtons.add((Button) findViewById(R.id.table54));

	}

	private int getGreenColor(int green)
	{
		return getResources().getColor(green);
	}

	private int getRedColor()
	{
		return getGreenColor(R.color.red);
	}

}

